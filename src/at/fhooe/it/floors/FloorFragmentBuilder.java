package at.fhooe.it.floors;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.Toast;
import at.fhooe.it.anim.GifDecoderView;
import at.fhooe.it.fourfloors.MenuActivity;
import at.fhooe.it.fourfloors.PopupQuestionActivity;
import at.fhooe.it.fourfloors.R;

/**
 * Manages the layouts from the fragments and react to pressing the buttons.
 * 
 * 
 * @author Team Four Floors
 * 
 */
public class FloorFragmentBuilder extends Fragment {

	private int mFloor;
	private int mRoom;
	private Button mBtnDown;
	private Button mBtnUp;
	private Button mBtnQuestion;
	private static int avatarToastMsgCounter = 1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// get arguments
		Bundle args = getArguments();
		avatarToastMsgCounter = 1;
		// set floor
		mFloor = args.getInt("floor", 1);
		mRoom = args.getInt("room", 0);

		// get n set layout
		int layout = args.getInt("layout");
		View rootView = inflater.inflate(layout, container, false);

		// set Avatar
		InputStream stream = null;
		try {
			if (mFloor <= 1) {
				stream = getActivity().getAssets().open("frog_splash.gif");
			} else if (mFloor == 2) {
				stream = getActivity().getAssets().open("frog_splash2.gif");
			} else if (mFloor == 3) {
				stream = getActivity().getAssets().open("frog_splash4.gif");
			} else if (mFloor == 4) {
				stream = getActivity().getAssets().open("frog_splash3.gif");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		ViewGroup avatarContainer = (ViewGroup) rootView
				.findViewById(R.id.room);
		GifDecoderView avatarView = new GifDecoderView(this.getActivity(),
				stream);

		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
				80, 80);
		relativeParams.setMargins(100, 260, 0, 0);
		relativeParams.width = 130;
		relativeParams.height = 130;
		avatarView.setLayoutParams(relativeParams);
		avatarView.setScaleType(ScaleType.FIT_XY);
		avatarView.setOnClickListener(new OnClickListener() {
			// setting helpful hints, when user presses on avatar
			@Override
			public void onClick(View v) {
				if (mFloor <= 1) {
					if (avatarToastMsgCounter == 1) {
						Toast.makeText(getActivity(),
								"press on the board to answer the question",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 2;
					} else if (avatarToastMsgCounter == 2) {
						Toast.makeText(
								getActivity(),
								"if you answer the question right, you can proceed to the next room",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 3;
					} else if (avatarToastMsgCounter == 3) {
						Toast.makeText(getActivity(), "stop pressing on me!",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 1;
					}
				} else if (mFloor == 2) {
					if (avatarToastMsgCounter == 1) {
						Toast.makeText(
								getActivity(),
								"press on the flip chart to answer the question",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 2;
					} else if (avatarToastMsgCounter == 2) {
						Toast.makeText(
								getActivity(),
								"if you answer the question right, you can proceed to the next room",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 3;
					} else if (avatarToastMsgCounter == 3) {
						Toast.makeText(getActivity(), "stop pressing on me!",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 1;
					}
				} else if (mFloor == 3) {
					if (avatarToastMsgCounter == 1) {
						Toast.makeText(
								getActivity(),
								"press on the white board to answer the question",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 2;
					} else if (avatarToastMsgCounter == 2) {
						Toast.makeText(
								getActivity(),
								"if you answer the question right, you can proceed to the next room",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 3;
					} else if (avatarToastMsgCounter == 3) {
						Toast.makeText(getActivity(), "stop pressing on me!",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 1;
					}

				} else if (mFloor == 4) {
					if (avatarToastMsgCounter == 1) {
						Toast.makeText(getActivity(),
								"press on the screen to answer the question",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 2;
					} else if (avatarToastMsgCounter == 2) {
						Toast.makeText(
								getActivity(),
								"if you answer the question right, you can proceed to the next room",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 3;
					} else if (avatarToastMsgCounter == 3) {
						Toast.makeText(getActivity(), "stop pressing on me!",
								Toast.LENGTH_LONG).show();
						avatarToastMsgCounter = 1;
					}
				}
			}
		});
		avatarContainer.addView(avatarView);

		// get n set buttons
		boolean bnDown = args.getBoolean("down", false);
		boolean bnUp = args.getBoolean("up", false);
		boolean bnQuestion = args.getBoolean("question", false);

		if (bnDown) {
			// down button
			mBtnDown = (Button) rootView.findViewById(R.id.bn_goDown);
			mBtnDown.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mFloor <= 1) {
						Intent goToMenu = new Intent().setClass(getActivity(),
								MenuActivity.class);
						goToMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

						if (MenuActivity.m_BackgroundMusicPlayer.isPlaying()) {
							MenuActivity.m_BackgroundMusicPlayer.stop();
						}
						startActivity(goToMenu);

					} else {
						Intent goToPreviousFloorIntent = new Intent().setClass(
								getActivity(), FloorActivity.class);
						goToPreviousFloorIntent.putExtra("floor", mFloor - 1);
						goToPreviousFloorIntent.putExtra("room", 4);
						goToPreviousFloorIntent.putExtra("down", true);
						startActivity(goToPreviousFloorIntent);

						endSound();
					}
				}
			});
		}
		if (bnUp) {
			// up button
			mBtnUp = (Button) rootView.findViewById(R.id.bn_upstairs_floor);
			mBtnUp.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					SharedPreferences sharedPrefs = getActivity()
							.getSharedPreferences(
									getResources().getString(
											R.string.game_state_key),
									Context.MODE_PRIVATE);
					int q = sharedPrefs.getInt(MenuActivity.OPEN_QUESTIONS, 1) - 1;
					Log.i(FloorFragmentBuilder.class.toString(), "open: " + q);
					Log.i(FloorFragmentBuilder.class.toString(), "roomq: "
							+ ((mFloor - 1) * 5 + mRoom));
					if (mFloor >= 4 || q <= ((mFloor - 1) * 5 + mRoom))
						return;
					Intent goToNextFloorIntent = new Intent().setClass(
							getActivity(), FloorActivity.class);
					goToNextFloorIntent.putExtra("floor", mFloor + 1);
					goToNextFloorIntent.putExtra("room", 0);
					goToNextFloorIntent.putExtra("up", true);
					startActivity(goToNextFloorIntent);

					endSound();
				}
			});
		}

		if (bnQuestion) {
			// question button, starts the PopupQuestion class
			mBtnQuestion = (Button) rootView.findViewById(R.id.bn_question);

			mBtnQuestion.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent goToQuestion = new Intent().setClass(getActivity(),
							PopupQuestionActivity.class);
					goToQuestion.putExtra("floor", mFloor);
					goToQuestion.putExtra("room", (mRoom % 5));
					startActivityForResult(goToQuestion,
							FloorActivity.QUESTION_REQUEST_CODE);
				}
			});
		}

		return rootView;
	}

	/**
	 * Set current room.
	 * 
	 * @param _room
	 *            value between 0 and 4
	 */
	public void setRoom(int _room) {
		mRoom = _room;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i(FloorFragmentBuilder.class.toString(), "result: " + resultCode);
	}

	/**
	 * Stop playing backgournd music.
	 */
	private void endSound() {
		// end sound
		if (MenuActivity.m_BackgroundMusicPlayer.isPlaying())
			MenuActivity.m_BackgroundMusicPlayer.stop();

		// release resources otherwise it will crash after restarting the app
		MenuActivity.m_BackgroundMusicPlayer.reset();
	}
}
