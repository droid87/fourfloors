package at.fhooe.it.floors;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.widget.Toast;
import at.fhooe.it.fourfloors.MenuActivity;
import at.fhooe.it.fourfloors.R;

/**
 * This class provides the application with all fragment which are used for the questions.
 * 
 * @author Team Four Floor
 *
 */
public class FloorActivity extends FragmentActivity {

	public static final int QUESTION_REQUEST_CODE = 1;
	public static final int ANSWER_RIGHT = 1;
	public static final int ANSWER_WRONG = 2;
	public static final int NO_ANSWER = 3;
	private int m_actualFloor = 1;
	private int m_actualRoom = 0;

	String background_music_inside = "groddle_theme_snappy_mix.mp3";

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	public static SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_floor);

		initBackgroundSound();
		// get floor nr (if non exist, set it to one)
		Bundle extras = getIntent().getExtras();
		m_actualFloor = extras.getInt("floor", 1);
		m_actualRoom = extras.getInt("room", 0);
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());
		
		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setCurrentItem(m_actualRoom);
		
		// check if we come from upon
		if (extras.getBoolean("down", false)) {
			if (m_actualFloor == 1 || m_actualFloor == 3) {
				mViewPager.setCurrentItem(4);
			} else {
				mViewPager.setCurrentItem(0);
			}
		} else if (extras.getBoolean("up", false)) {
			if (m_actualFloor == 1 || m_actualFloor == 3) {
				mViewPager.setCurrentItem(0);
			} else {
				mViewPager.setCurrentItem(4);
			}
		}

		//for getting the right room number
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int _room) {
				Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
				((FloorFragmentBuilder) page).setRoom(_room);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {}
		});
	}

	/**
	 * Set the background sound.
	 */
	private void initBackgroundSound() {
		AssetFileDescriptor afd;
		MenuActivity.m_BackgroundMusicPlayer = new MediaPlayer();
		try {

			afd = getAssets().openFd(background_music_inside);
			MenuActivity.m_BackgroundMusicPlayer.setLooping(true);
			MenuActivity.m_BackgroundMusicPlayer.setDataSource(
					afd.getFileDescriptor(), afd.getStartOffset(),
					afd.getLength());

		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			MenuActivity.m_BackgroundMusicPlayer.prepare();

		} catch (Exception e) {
			e.printStackTrace();
		}
		MenuActivity.m_BackgroundMusicPlayer
				.setOnCompletionListener(new OnCompletionListener() {

					@Override
					public void onCompletion(MediaPlayer mp) {

					}
				});

		MenuActivity.m_BackgroundMusicPlayer.start();
	}

	@Override
	protected void onPause() {
		//stop playing if we are onPause 
		Context context = getApplicationContext();
		ActivityManager am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> taskInfo = am.getRunningTasks(1);
		if (!taskInfo.isEmpty()) {
			ComponentName topActivity = taskInfo.get(0).topActivity;
			if (!topActivity.getPackageName().equals(context.getPackageName())) {
				if (MenuActivity.m_BackgroundMusicPlayer.isPlaying()) {
					MenuActivity.m_BackgroundMusicPlayer.stop();
				}
			}
		}
		super.onPause();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == QUESTION_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				int answer = intent.getExtras().getInt("answer", NO_ANSWER);
				if (answer == ANSWER_RIGHT) {

					Toast.makeText(this, "right Answer!", Toast.LENGTH_SHORT)
							.show();
				} else if (answer == ANSWER_WRONG) {
					Toast.makeText(this, "wrongAnswer!", Toast.LENGTH_SHORT)
							.show();
				}
			}
		}
		
		super.onActivityResult(requestCode, resultCode, intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.floor, menu);
		return true;
	}

	/**
	 * This inner class manages the fragments.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		/**
		 * Constructor.
		 * 
		 * @param fm
		 */
		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Bundle args = new Bundle();
			args.putInt("floor", m_actualFloor);
			args.putInt("room", m_actualRoom);
			args.putBoolean("question", true);

			switch (m_actualFloor) {
			case 1: // first floor
				if (position == 0) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_1_floor01);
					args.putBoolean("down", true);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 1) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_1_floor02);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 2) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_1_floor03);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 3) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_1_floor04);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 4) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_1_floor05);
					args.putBoolean("up", true);
					fragment.setArguments(args);

					return fragment;
				}
				break;

			case 2: // second floor
				if (position == 0) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_2_floor05);
					args.putBoolean("down", true);

					fragment.setArguments(args);

					return fragment;
				} else if (position == 1) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_2_floor02);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 2) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_2_floor03);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 3) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_2_floor04);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 4) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_2_floor01);
					args.putBoolean("up", true);
					fragment.setArguments(args);

					return fragment;
				}
				break;

			case 3: // third floor
				if (position == 0) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_3_floor01);
					args.putBoolean("down", true);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 1) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_3_floor02);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 2) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_3_floor03);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 3) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_3_floor04);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 4) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_3_floor05);
					args.putBoolean("up", true);
					fragment.setArguments(args);

					return fragment;
				}
				break;

			case 4: // fourth floor
				if (position == 0) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_4_floor05);
					args.putBoolean("down", true);

					fragment.setArguments(args);

					return fragment;
				} else if (position == 1) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_4_floor02);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 2) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_4_floor03);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 3) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_4_floor04);
					fragment.setArguments(args);

					return fragment;
				} else if (position == 4) {
					Fragment fragment = new FloorFragmentBuilder();
					args.putInt("layout", R.layout.fragment_4_floor01);
					fragment.setArguments(args);

					return fragment;
				}
				break;
			}

			return null;
		}

		@Override
		public int getCount() {
			// return the opened questions
			// check in which floor we actually are and if we on the last one, we have to return the amount of open questions
			SharedPreferences sharedPrefs = getSharedPreferences(
					getResources().getString(R.string.game_state_key),
					Context.MODE_PRIVATE);
			int open = sharedPrefs.getInt(MenuActivity.OPEN_QUESTIONS, 1);
			float x = open/5f;
			int calc = 0;
			
			if(x>m_actualFloor){
				return 5;
			}else{
				calc = open%5;
				if(calc==0 && open>=5)
					return 5;
			}
			open %= 5;
			return open;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();

			switch (m_actualFloor) {
			case 1: // first floor
				switch (position) {
				case 0:
					return getString(R.string.title_section1).toUpperCase(l);
				case 1:
					return getString(R.string.title_section2).toUpperCase(l);
				case 2:
					return getString(R.string.title_section3).toUpperCase(l);
				case 3:
					return getString(R.string.title_section4).toUpperCase(l);
				case 4:
					return getString(R.string.title_section5).toUpperCase(l);
				}
				break;

			case 2: // second floor
				switch (position) {
				case 0:
					return getString(R.string.title_section1).toUpperCase(l);
				case 1:
					return getString(R.string.title_section2).toUpperCase(l);
				case 2:
					return getString(R.string.title_section3).toUpperCase(l);
				case 3:
					return getString(R.string.title_section4).toUpperCase(l);
				case 4:
					return getString(R.string.title_section5).toUpperCase(l);
				}
				break;

			case 3: // third floor
				switch (position) {
				case 0:
					return getString(R.string.title_section1).toUpperCase(l);
				case 1:
					return getString(R.string.title_section2).toUpperCase(l);
				case 2:
					return getString(R.string.title_section3).toUpperCase(l);
				case 3:
					return getString(R.string.title_section4).toUpperCase(l);
				case 4:
					return getString(R.string.title_section5).toUpperCase(l);
				}
				break;

			case 4: // fourth floor
				switch (position) {
				case 0:
					return getString(R.string.title_section1).toUpperCase(l);
				case 1:
					return getString(R.string.title_section2).toUpperCase(l);
				case 2:
					return getString(R.string.title_section3).toUpperCase(l);
				case 3:
					return getString(R.string.title_section4).toUpperCase(l);
				case 4:
					return getString(R.string.title_section5).toUpperCase(l);
				}
				break;
			}

			return null;
		}
	}

	@Override
	public void onBackPressed() {
		//react to back button
		Intent goToMenu = new Intent().setClass(getApplicationContext(),
				MenuActivity.class);
		goToMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		if (MenuActivity.m_BackgroundMusicPlayer.isPlaying()) {
			MenuActivity.m_BackgroundMusicPlayer.stop();
		}
		startActivity(goToMenu);
	}

	@Override
	public void finish() {
		// end sound
		if (MenuActivity.m_BackgroundMusicPlayer.isPlaying())
			MenuActivity.m_BackgroundMusicPlayer.stop();

		// release resources otherwise it will crash after restarting the app
		MenuActivity.m_BackgroundMusicPlayer.reset();

		super.finish();
	}
}
