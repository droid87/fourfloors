package at.fhooe.it.sql;

/**
 * Stores all data needed for a question.
 * 
 * @author Team Four Floors
 *
 */
public class Question {

	public long id;
	public String question;
	public String info;
	public int floor;
	public int room;
	public String answer1;
	public String answer2;
	public String answer3;
	public String answer4;
	public int correct;
	
	
	/**
	 * Constructor without id.
	 * 
	 * @param _question question itself
	 * @param _info info to gather some more information to the question
	 * @param _floor located floor of the question
	 * @param _room located room of the question
	 * @param _answer1 possible answer
	 * @param _answer2 possible answer
	 * @param _answer3 possible answer
	 * @param _answer4 possible answer
	 * @param _correct correct answer
	 */
	public Question(String _question, String _info, int _floor, int _room, String _answer1, String _answer2, String _answer3, String _answer4, int _correct){
		this.question = _question;
		this.info = _info;
		this.floor = _floor;
		this.room = _room;
		this.answer1 = _answer1;
		this.answer2 = _answer2;
		this.answer3 = _answer3;
		this.answer4 = _answer4;
		this.correct = _correct;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param __id id of sqlite3 database
	 * @param _question question itself
	 * @param _info info to gather some more information to the question
	 * @param _floor located floor of the question
	 * @param _room located room of the question
	 * @param _answer1 possible answer
	 * @param _answer2 possible answer
	 * @param _answer3 possible answer
	 * @param _answer4 possible answer
	 * @param _correct correct answer
	 */
	public Question(long _id, String _question, String _info, int _floor, int _room, String _answer1, String _answer2, String _answer3, String _answer4, int _correct){
		this.id = _id;
		this.question = _question;
		this.info = _info;
		this.floor = _floor;
		this.room = _room;
		this.answer1 = _answer1;
		this.answer2 = _answer2;
		this.answer3 = _answer3;
		this.answer4 = _answer4;
		this.correct = _correct;
	}
	
	@Override
	public String toString() {
		return this.question;
	}
}
