package at.fhooe.it.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * SQL Connector with all strings we needed.
 * 
 * @author Team Four Floors
 *
 */
public class SQLQuery extends SQLiteOpenHelper {

	public static final String TABLE_QUESTIONS = "questions";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_QUESTION = "question";
	public static final String COLUMN_FLOOR = "floor";
	public static final String COLUMN_ROOM = "room";
	public static final String COLUMN_ANSWER_1 = "ans1";
	public static final String COLUMN_ANSWER_2 = "ans2";
	public static final String COLUMN_ANSWER_3 = "ans3";
	public static final String COLUMN_ANSWER_4 = "ans4";
	public static final String COLUMN_CORRECT = "correct";
	public static final String COLUMN_INFO = "info";

	private static final String DATABASE_NAME = "questions.db";
	private static final int DATABASE_VERSION = 1;

	/**
	 * Database creation string.
	 */
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_QUESTIONS + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_QUESTION
			+ " text not null, " + COLUMN_FLOOR + " integer, "
			+ COLUMN_ANSWER_1 + " text not null, " + COLUMN_ANSWER_2
			+ " text not null, " + COLUMN_ANSWER_3 + " text not null, "
			+ COLUMN_ANSWER_4 + " text not null, " + COLUMN_INFO
			+ " text not null, " + COLUMN_CORRECT + " integer, " + COLUMN_ROOM
			+ " integer);";

	/**
	 * Constructor.
	 * 
	 * @param _context
	 */
	public SQLQuery(Context _context) {
		super(_context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(SQLQuery.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONS);
		onCreate(db);
	}
}
