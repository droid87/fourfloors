package at.fhooe.it.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Helper to handle the sqlite3 database.
 * Basic read/write.
 * 
 * @author Team Four Floors
 *
 */
public class SQLiteQuestions {
	// Database fields
	private SQLiteDatabase database;
	private SQLQuery dbHelper;
	private String[] allColumns = { SQLQuery.COLUMN_ID,
			SQLQuery.COLUMN_QUESTION, SQLQuery.COLUMN_INFO,
			SQLQuery.COLUMN_FLOOR, SQLQuery.COLUMN_ROOM,
			SQLQuery.COLUMN_ANSWER_1, SQLQuery.COLUMN_ANSWER_2,
			SQLQuery.COLUMN_ANSWER_3, SQLQuery.COLUMN_ANSWER_4,
			SQLQuery.COLUMN_CORRECT };

	/**
	 * Constructor.
	 * 
	 * @param _context
	 */
	public SQLiteQuestions(Context _context) {
		dbHelper = new SQLQuery(_context);
	}

	/**
	 * Open the database.
	 * 
	 * @throws SQLException
	 */
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	/**
	 * Close the database.
	 */
	public void close() {
		dbHelper.close();
	}

	/**
	 * Returns id of question in table. if it fails it will return -1
	 * 
	 * @param _question
	 * @return id or -1
	 */
	public long addQuestion(Question _question) {
		ContentValues values = new ContentValues();
		values.put(SQLQuery.COLUMN_QUESTION, _question.question);
		values.put(SQLQuery.COLUMN_INFO, _question.info);
		values.put(SQLQuery.COLUMN_FLOOR, _question.floor);
		values.put(SQLQuery.COLUMN_ROOM, _question.room);
		values.put(SQLQuery.COLUMN_ANSWER_1, _question.answer1);
		values.put(SQLQuery.COLUMN_ANSWER_2, _question.answer2);
		values.put(SQLQuery.COLUMN_ANSWER_3, _question.answer3);
		values.put(SQLQuery.COLUMN_ANSWER_4, _question.answer4);
		values.put(SQLQuery.COLUMN_CORRECT, _question.correct);

		return database.insert(SQLQuery.TABLE_QUESTIONS, null, values);
	}

	/**
	 * Returns number of affected rows
	 * 
	 * @param _question
	 * @return affected rows
	 */
	public int deleteComment(Question _question) {
		return database.delete(SQLQuery.TABLE_QUESTIONS, SQLQuery.COLUMN_ID
				+ " = " + _question.id, null);
	}

	/**
	 * Return the question of a specific floor/room.
	 * 
	 * @param _floor
	 * @param _room
	 * @return question
	 */
	public Question getQuestion(int _floor, int _room) {
		Cursor cursor = database.rawQuery("select * from "
				+ SQLQuery.TABLE_QUESTIONS + " where " + SQLQuery.COLUMN_FLOOR
				+ "=? and " + SQLQuery.COLUMN_ROOM + "=?", new String[] {
				String.valueOf(_floor), String.valueOf(_room) });
		if (cursor.getCount() <= 0)
			return null;
		
		cursor.moveToFirst();
		
		return cursorToQuestion(cursor);
	}

	/**
	 * All entries of database.
	 * 
	 * @return count of database entries
	 */
	public int count() {
		Cursor cursor = database.query(SQLQuery.TABLE_QUESTIONS, allColumns,
				null, null, null, null, null);

		return cursor.getCount();
	}

	/**
	 * Returns a Question object from a cursor position.
	 * 
	 * @param _cursor
	 * @return question
	 */
	private Question cursorToQuestion(Cursor _cursor) {
		return new Question(
				_cursor.getLong(_cursor.getColumnIndex(SQLQuery.COLUMN_ID)),
				_cursor.getString(_cursor
						.getColumnIndex(SQLQuery.COLUMN_QUESTION)),
				_cursor.getString(_cursor.getColumnIndex(SQLQuery.COLUMN_INFO)),
				_cursor.getInt(_cursor.getColumnIndex(SQLQuery.COLUMN_FLOOR)),
				_cursor.getInt(_cursor.getColumnIndex(SQLQuery.COLUMN_ROOM)),
				_cursor.getString(_cursor
						.getColumnIndex(SQLQuery.COLUMN_ANSWER_1)), _cursor
						.getString(_cursor
								.getColumnIndex(SQLQuery.COLUMN_ANSWER_2)),
				_cursor.getString(_cursor
						.getColumnIndex(SQLQuery.COLUMN_ANSWER_3)), _cursor
						.getString(_cursor
								.getColumnIndex(SQLQuery.COLUMN_ANSWER_4)),
				_cursor.getInt(_cursor.getColumnIndex(SQLQuery.COLUMN_CORRECT)));
	}
}
