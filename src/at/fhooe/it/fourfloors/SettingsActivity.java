package at.fhooe.it.fourfloors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ToggleButton;

/**
 * Let the user set if he wants to turn on/off the music and let him switch the
 * language.
 * 
 * @author Team Four Floors
 * 
 */
public class SettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_settings);
		setupActionBar();
		// toggle music
		final ToggleButton btn = (ToggleButton) findViewById(R.id.toggleMusic);
		if (MenuActivity.m_BackgroundMusicPlayer.isPlaying())
			btn.setChecked(true);

		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (btn.isChecked()) {
					// turn music on
					try {
						MenuActivity.m_BackgroundMusicPlayer.prepare();

					} catch (Exception e) {
						e.printStackTrace();
					}
					MenuActivity.m_BackgroundMusicPlayer.start();

				} else {
					// turn music off
					MenuActivity.m_BackgroundMusicPlayer.stop();
				}
			}
		});

		// check actual language and let the correct radiobutton be checked
		SharedPreferences sharedPrefs = getSharedPreferences(getResources()
				.getString(R.string.game_state_key), Context.MODE_PRIVATE);

		final RadioGroup group = (RadioGroup) findViewById(R.id.radioGroup1);
		final RadioButton ger = (RadioButton) findViewById(R.id.radioGerman);
		final RadioButton eng = (RadioButton) findViewById(R.id.radioEnglish);

		if (sharedPrefs.getString(MenuActivity.LANGUAGE, "en_US")
				.equalsIgnoreCase("de_AT")) {
			ger.setChecked(true);
		}

		OnClickListener listener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				// change language to application and restart it
				int selectedId = group.getCheckedRadioButtonId();

				SharedPreferences sharedPrefs = getSharedPreferences(
						getResources().getString(R.string.game_state_key),
						Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sharedPrefs.edit();

				if (selectedId == R.id.radioEnglish) {
					editor.putString(MenuActivity.LANGUAGE, "en_US");

				} else {
					editor.putString(MenuActivity.LANGUAGE, "de_AT");
				}

				editor.commit();

				// restart app
				Thread t = new Thread() {
					public void run() {
						Intent i = getBaseContext().getPackageManager()
								.getLaunchIntentForPackage(
										getBaseContext().getPackageName());
						startActivity(i);
					};
				};
				t.start();
				System.exit(0);
			}
		};

		ger.setOnClickListener(listener);
		eng.setOnClickListener(listener);

		// react on back button
		Button back = (Button) findViewById(R.id.settings_back);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	private void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		} else if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

}
