package at.fhooe.it.fourfloors;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;
import at.fhooe.it.sql.Question;
import at.fhooe.it.sql.SQLiteQuestions;

/**
 * The first activity which the user see after the splash screen. Here he can
 * navigate to settings, start or reset the game.
 * 
 * @author Team Four Floor
 * 
 */
public class MenuActivity extends Activity {

	final static int CLOSE_DURATIOIN = 2000;
	public final static String OPEN_QUESTIONS = "openQuestions";
	public final static String LANGUAGE = "lang";
	long mLastPressedBackButton;
	String background_music_1 = "groddle_theme_snappy_mix.mp3";
	String background_music_2 = "groddle_theme_dreamland_mix.mp3";
	public static MediaPlayer m_BackgroundMusicPlayer = new MediaPlayer();

	Button bn_play;
	Button bn_reset;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// check language
		SharedPreferences sharedPrefs = getSharedPreferences(getResources()
				.getString(R.string.game_state_key), Context.MODE_PRIVATE);
		Locale locale = new Locale(sharedPrefs.getString(MenuActivity.LANGUAGE,
				"en_US"));
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getApplicationContext().getResources()
				.updateConfiguration(config, null);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		if (!m_BackgroundMusicPlayer.isPlaying()) {

			initBackgroundSound();
		}

		bn_play = (Button) findViewById(R.id.bn_play);
		bn_reset = (Button) findViewById(R.id.bn_reset);

		bn_play.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				playButtonSound();
				Intent intentBuilding = new Intent().setClass(
						getApplicationContext(), BuildingActivity.class);
				startActivity(intentBuilding);

			}
		});
		bn_reset.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				playButtonSound();
				resetGame();

			}
		});

		initDatabase();
	}

	/**
	 * Sets up the database if there are no entries. (only called the first time
	 * or if the user clear the applications data)
	 */
	private void initDatabase() {
		// TODO
		SQLiteQuestions q = new SQLiteQuestions(getApplicationContext());
		q.open();
		Log.i(MenuActivity.class.getName(), "count: " + q.count());
		if (q.count() <= 1) {
			q.addQuestion(new Question(
					"What doesn't fit into the analysis phase?", "", 1, 0,
					"Brainstorming", "Waterboarding", "Mind mapping",
					"Storyboards", 2));

			q.addQuestion(new Question(
					"What are none of the goals of brainstorming?", "", 1, 1,
					"Creative thinking", "Generate ideas", "New associations",
					"Reach transcendency", 4));

			q.addQuestion(new Question(
					"What are none commonly used data collection techniques for ethnographic observations?",
					"", 1, 2, "Notes", "Diaries", "Endoscopies", "Videos", 3));

			q.addQuestion(new Question(
					"What is an disadvantage of focus groups?", "", 1, 3,
					"Range of information", "Cheap & easy",
					"In-depth information", "Sampling is not random", 4));

			q.addQuestion(new Question(
					"What are not an element of a storyboard?", "", 1, 4,
					"Level of detail", "Inclusion of text", "Number of frames",
					"Easter eggs", 4));

			q.addQuestion(new Question(
					"What doesn't belong into the Design Phase?", "", 2, 0,
					"Affordances", "Task analysis", "Bug fixes",
					"Theoretical Models", 3));

			q.addQuestion(new Question(
					"The designer's intended model for the use of the system, that is given to the user through the design and interface is called:",
					"", 2, 1, "Conceptual Model", "Mental Model",
					"Design Model", "View Model", 1));

			q.addQuestion(new Question(
					"Fitts's law is a model of human movement that predicts that the time required to rapidly move to a target area is a function of the distance to the target and the size of the ...?",
					"", 2, 2, "Mouse pointer", "Target", "Display", "Subpixel",
					2));

			q.addQuestion(new Question(
					"Higgs Law describes that if we increase the number of choices it will increase the decision time ...",
					"", 2, 3, "Minimaly", "Linearly", "Exponentially",
					"Logarithmically", 4));

			q.addQuestion(new Question(
					"What should be one of the goal to archive for screen designs?",
					"", 2, 4, "Balanced", "Rare", "Small", "Big", 1));

			long q11 = q
					.addQuestion(new Question(
							"Don Norman introduced many important principles of good design. Which one of the following doesn`t belong here?",
							"hey, some Info",
							3,
							0,
							"stage and action alternatives should be always visible",
							"good conceptual model with a consistent system image",
							"interface should include good mappings that show the relationship between stages",
							"collect continuous feedback of the user", 4));
			long q12 = q
					.addQuestion(new Question(
							"Is it important to take human capabilities into account, when desinging a system/software-product?",
							"hey, some Info",
							3,
							1,
							"no, its not important, because todays technologies compensate all human inperfections",
							"yes, otherwise people won�t be able to use it, or the user performance will be sub optimal",
							"yes, it�s important in order to save the environment",
							"as long as the software product works, it�s not important",
							2));
			long q13 = q
					.addQuestion(new Question(
							"How much resolution does the eye need, if the viewing distance equals the horiz. image widht, the horiz. Viewangle equals 2*atan 0,5=53� and the maximal angular resolution of the eye is 1/60�?",
							"hey, some Info", 3, 2, "100dpi", "200dpi",
							"300dpi", "600dpi", 3));
			long q14 = q.addQuestion(new Question("What does STSS stand for?",
					"hey, some Info", 3, 3, "Short-Term Sensory Storage",
					"Strongly Time shifted Simulation",
					"Short-Time Sensible Storage",
					"Short-Term Sensible Storage", 1));
			long q15 = q
					.addQuestion(new Question(
							"The working memory can store memories for up to ...",
							"hey, some Info",
							3,
							4,
							"memories for up to 5 seconds, can be accessed in ~10ms ",
							"memories for up to 30 seconds, can be accessed in ~70ms ",
							"...memories for up to 10 minutes, can be accessed in ~10ms",
							"...memories for up to 2 hours, can be accessed in ~70ms",
							2));

			/*
			 * Questions 4. Floor
			 */
			long q16 = q
					.addQuestion(new Question(
							"Select the right/best fitting statement: The overarching goal of the Deployment Phase is ...",
							"hey, some Info",
							4,
							0,
							"... to place the solution into a production environment, test and evaluate the product",
							"... to distribute and advertise the product",
							"... to do final bug-fixes and performance-testing",
							"... to deploy the solution for the customer, including installation, set-up and service",
							1));
			long q17 = q
					.addQuestion(new Question(
							"Select the incorrect statement:",
							"hey, some Info",
							4,
							1,
							"Diary Studies are suitable for non-workplace, non-controlled or on-the-go settings",
							"Participants of Diary Studies always  follow through and record a sufficient number of entries",
							"Time recording in Diary Studies is more accurate than e.g. in a survey",
							"Diary studies are more intrusive than e.g. a survey",
							2));
			long q18 = q
					.addQuestion(new Question(
							"Select the incorrect answer: Usability Evaluation is seldom applied in reality, because�",
							"hey, some Info", 4, 2,
							"...of fear of additional costs",
							"...of release deadlines",
							"...it's necessity is neglected",
							"...no participants are found", 4));
			long q19 = q
					.addQuestion(new Question(
							"Select the correct order of the Phases of a Field Study:(Goal Definition - GD, Study Design - SD, Participants and Task Selection - PTS, Study �Beta Test� &Conduction - SBTC, Data Collextion and Analysis - DCA)",
							"hey, some Info", 4, 3, "GD, DCA, SD, PTS, SBTC",
							"GD, SD, PTS, SBTC, DCA",
							"DCA, GD, PTS,  SBTC, DCA",
							"DCA, GD, SD, PTS, SBTC", 2));
			long q20 = q.addQuestion(new Question(
					"Likert Scales are used to...", "hey, some Info", 4, 4,
					"...measure options", "...display participants profile",
					"...indicate data value", "...rate lectures", 1));

		}
		q.close();
	}

	/**
	 * Resets the game to the starting point. Basically it removes the shared
	 * preferences after it asks the user if he is really willing to do that.
	 */
	protected void resetGame() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:

					SharedPreferences sharedPrefs = getSharedPreferences(
							getResources().getString(R.string.game_state_key),
							Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = sharedPrefs.edit();
					editor.putInt(MenuActivity.OPEN_QUESTIONS, 1);
					editor.commit();

					Log.i(MenuActivity.class.toString(), "pressed yes");
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					// No button clicked- do nothing
					Log.i(MenuActivity.class.toString(), "pressed no");
					break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Are you sure to reset the game?")
				.setPositiveButton("Yes", dialogClickListener)
				.setNegativeButton("No", dialogClickListener).show();
	}

	@Override
	protected void onPause() {
		if (this.isFinishing()) { // basically BACK was pressed from this
									// activity
			if (m_BackgroundMusicPlayer.isPlaying()) {
				m_BackgroundMusicPlayer.stop();
			}
		}
		Context context = getApplicationContext();
		ActivityManager am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> taskInfo = am.getRunningTasks(1);
		if (!taskInfo.isEmpty()) {
			ComponentName topActivity = taskInfo.get(0).topActivity;
			if (!topActivity.getPackageName().equals(context.getPackageName())) {
				if (m_BackgroundMusicPlayer.isPlaying()) {
					m_BackgroundMusicPlayer.stop();
				}
			}
		}
		super.onPause();
	}

	/**
	 * Start playing the button sound.
	 */
	protected void playButtonSound() {
		MediaPlayer.create(MenuActivity.this, R.raw.piggy_plop).start();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent settings = new Intent(getApplicationContext(),
					SettingsActivity.class);
			this.startActivity(settings);
			break;

		case R.id.help:
			Intent help = new Intent(getApplicationContext(),
					HelpActivity.class);
			this.startActivity(help);
			break;
		}

		return true;
	}

	/**
	 * Initialize the background sound.
	 */
	private void initBackgroundSound() {
		MenuActivity.m_BackgroundMusicPlayer = new MediaPlayer();
		AssetFileDescriptor afd;
		try {

			afd = getAssets().openFd(background_music_2);
			m_BackgroundMusicPlayer.setLooping(true);
			m_BackgroundMusicPlayer.setDataSource(afd.getFileDescriptor(),
					afd.getStartOffset(), afd.getLength());
			m_BackgroundMusicPlayer.prepare();

		} catch (Exception e) {
			e.printStackTrace();
		}

		m_BackgroundMusicPlayer.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		// explicit ask the user gently if he want to leave
		// only because we return to the main menu if he presses the back button
		// once and not going back to the building activity
		long time = Calendar.getInstance().getTimeInMillis();
		if ((time - mLastPressedBackButton) < CLOSE_DURATIOIN) {
			finish();

		} else {
			mLastPressedBackButton = time;
			Toast.makeText(getApplicationContext(),
					"press back again to leave", CLOSE_DURATIOIN).show();
		}

	}

	@Override
	public void finish() {
		// end sound
		if (m_BackgroundMusicPlayer.isPlaying())
			m_BackgroundMusicPlayer.stop();

		// release resources otherwise it will crash after restarting the app
		m_BackgroundMusicPlayer.reset();

		super.finish();
	}

	@Override
	protected void onResume() {
		if (m_BackgroundMusicPlayer.isPlaying()) {

		} else {
			initBackgroundSound();
		}
		super.onResume();
	}
}
