package at.fhooe.it.fourfloors;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.ViewGroup;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import at.fhooe.it.anim.GifDecoderView;

/**
 * Shows the user that the game is starting.
 * 
 * @author Team Four Floors
 * 
 */

public class SplashScreenActivity extends Activity {

	ViewGroup avatarContainer;
	GifDecoderView avatarView;
	private static final int SPLASH_TIME_OUT = 5000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);

		InputStream stream = null;
		try {
			stream = getAssets().open("frog_splash.gif");
		} catch (IOException e) {
			e.printStackTrace();
		}

		avatarContainer = (ViewGroup) findViewById(R.id.avatarContainer);
		avatarView = new GifDecoderView(this, stream);
		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
				80, 80);
		relativeParams.width = 130;
		relativeParams.height = 130;
		avatarView.setLayoutParams(relativeParams);
		avatarView.setScaleType(ScaleType.FIT_XY);
		avatarContainer.addView(avatarView);

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				Intent i = new Intent(SplashScreenActivity.this,
						MenuActivity.class);
				startActivity(i);

				// close this activity
				finish();
			}
		}, SPLASH_TIME_OUT);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash_screen, menu);
		return true;
	}
}
