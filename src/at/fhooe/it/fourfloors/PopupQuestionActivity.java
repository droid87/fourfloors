package at.fhooe.it.fourfloors;

import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import at.fhooe.it.floors.FloorActivity;
import at.fhooe.it.sql.Question;
import at.fhooe.it.sql.SQLiteQuestions;

/**
 * Presents the question to the user and also handles the answers.
 * 
 * @author Team Four Floors
 * 
 */
public class PopupQuestionActivity extends Activity implements OnClickListener {

	private Question mQuestion;
	int m_floor;
	int m_room;
	Button bn_A, bn_B, bn_C, bn_D;
	TextView tv_Title, tv_Question;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		m_floor = getIntent().getExtras().getInt("floor", 1);
		m_room = getIntent().getExtras().getInt("room", 1);

		// differ floors (look different)
		switch (m_floor) {
		case 1:
		default:
			setContentView(R.layout.question_popup_01);
			break;
		case 2:
			setContentView(R.layout.question_popup_02);
			break;
		case 3:
			setContentView(R.layout.question_popup_03);
			break;
		case 4:
			setContentView(R.layout.question_popup_04);
			break;
		}
		initView();
		SQLiteQuestions quests = new SQLiteQuestions(getApplicationContext());
		quests.open();
		mQuestion = quests.getQuestion(m_floor, m_room);
		quests.close();
		if (mQuestion == null) {
			Log.d(getTitle().toString(), "Question is null");
			return;

		} else {
			populateView();
		}
	}

	/**
	 * Initialize all views and set OnClickListener to the buttons.
	 */
	private void initView() {
		bn_A = (Button) findViewById(R.id.bn_answer_a);
		bn_B = (Button) findViewById(R.id.bn_answer_b);
		bn_C = (Button) findViewById(R.id.bn_answer_c);
		bn_D = (Button) findViewById(R.id.bn_answer_d);
		tv_Question = (TextView) findViewById(R.id.tv_Question);
		tv_Title = (TextView) findViewById(R.id.tv_Title);

		bn_A.setOnClickListener(this);
		bn_B.setOnClickListener(this);
		bn_C.setOnClickListener(this);
		bn_D.setOnClickListener(this);
	}

	/**
	 * Fill the views with the data from the question.
	 */
	private void populateView() {
		bn_A.setText("A: " + mQuestion.answer1);
		bn_B.setText("B: " + mQuestion.answer2);
		bn_C.setText("C: " + mQuestion.answer3);
		bn_D.setText("D: " + mQuestion.answer4);
		tv_Question.setText(mQuestion.question);
		switch (mQuestion.floor) {
		case 1:
			tv_Title.setText("Analysis Phase" + "- Room " + mQuestion.room);
			break;
		case 2:
			tv_Title.setText("Design Phase" + "- Room " + mQuestion.room);
			break;
		case 3:
			tv_Title.setText("Implementation Phase" + "- Room "
					+ mQuestion.room);
			break;
		case 4:
			tv_Title.setText("Deployment Phase" + "- Room " + mQuestion.room);
			break;

		}
	}

	/**
	 * Called if user answers wrong and leave the activity.
	 */
	private void wrongAnswer() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("That was not the correct answer.\nTry it again!")
			.setCancelable(false)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					setResult(FloorActivity.ANSWER_WRONG);
				}
			})
			.show();
	}

	/**
	 * Called if user answers correct and save the new state before the activity
	 * gets closed.
	 */
	private void rightAnswer() {
		SharedPreferences sharedPrefs = getSharedPreferences(getResources()
				.getString(R.string.game_state_key), Context.MODE_PRIVATE);

		int openWindowsCount = sharedPrefs.getInt(MenuActivity.OPEN_QUESTIONS,
				1);

		SharedPreferences.Editor editor = sharedPrefs.edit();
		int newOpenWindowsCount = (openWindowsCount + 1);
		editor.putInt(MenuActivity.OPEN_QUESTIONS, newOpenWindowsCount);
		editor.commit();
		FloorActivity.mSectionsPagerAdapter.notifyDataSetChanged();

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Congratulation!\nSwipe to the next room")
			.setCancelable(false)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					setResult(FloorActivity.ANSWER_RIGHT);
					finish();
				}
			})
			.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.popup_question, menu);
		return true;
	}

	@Override
	protected void onPause() {
		//stop the sound onPause
		Context context = getApplicationContext();
		ActivityManager am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> taskInfo = am.getRunningTasks(1);
		if (!taskInfo.isEmpty()) {
			ComponentName topActivity = taskInfo.get(0).topActivity;
			if (!topActivity.getPackageName().equals(context.getPackageName())) {
				if (MenuActivity.m_BackgroundMusicPlayer.isPlaying()) {
					MenuActivity.m_BackgroundMusicPlayer.stop();
				}
			}
		}
		super.onPause();
	}

	@Override
	public void onClick(View v) {
		int ans = 0;
		if (v.getId() == bn_A.getId()) {
			ans = 1;
		} else if (v.getId() == bn_B.getId()) {
			ans = 2;
		} else if (v.getId() == bn_C.getId()) {
			ans = 3;
		} else if (v.getId() == bn_D.getId()) {
			ans = 4;
		}

		//check if clicked button is the right one
		if (mQuestion.correct == ans) {
			rightAnswer();
		} else {
			wrongAnswer();
		}
	}
}
