package at.fhooe.it.fourfloors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import at.fhooe.it.floors.FloorActivity;

/**
 * Shows the building to select a window for entering a room.
 * 
 * @author Team Four Floors
 * 
 */
public class BuildingActivity extends Activity {

	ScrollView sv_Building;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_building);
		setupActionBar();

		sv_Building = (ScrollView) findViewById(R.id.scrollViewBuilding);

		sv_Building.post(new Runnable() {

			@Override
			public void run() {
				sv_Building.fullScroll(ScrollView.FOCUS_DOWN);
			}
		});
	}

	@Override
	protected void onResume() {
		setupWindows();
		super.onResume();
	}

	/**
	 * Checks shared preferences and open the windows on each room which are
	 * already answered.
	 */
	private void setupWindows() {
		SharedPreferences sharedPrefs = getSharedPreferences(getResources()
				.getString(R.string.game_state_key), Context.MODE_PRIVATE);
		int openWindowsCount = sharedPrefs.getInt(MenuActivity.OPEN_QUESTIONS,
				1);

		for (int i = 0; i < openWindowsCount; i++) {
			if (i < 5) {
				LinearLayout firstFloor = (LinearLayout) findViewById(R.id.first_floor);
				ImageButton imgButton = (ImageButton) firstFloor.getChildAt(i);
				imgButton.setImageResource(R.drawable.window1);
				imgButton.setClickable(true);

			} else if (i >= 5 && i < 10) {
				LinearLayout firstFloor = (LinearLayout) findViewById(R.id.second_floor);
				ImageButton imgButton = (ImageButton) firstFloor
						.getChildAt(i - 5);
				imgButton.setImageResource(R.drawable.window2);
				imgButton.setClickable(true);

			} else if (i >= 10 && i < 15) {
				LinearLayout firstFloor = (LinearLayout) findViewById(R.id.third_floor);
				ImageButton imgButton = (ImageButton) firstFloor
						.getChildAt(i - 10);
				imgButton.setImageResource(R.drawable.window3);
				imgButton.setClickable(true);

			} else if (i >= 15 && i < 20) {
				LinearLayout firstFloor = (LinearLayout) findViewById(R.id.forth_floor);
				ImageButton imgButton = (ImageButton) firstFloor
						.getChildAt(i - 15);
				imgButton.setImageResource(R.drawable.window4);
				imgButton.setClickable(true);
			}
		}

		for (int j = 20; j >= openWindowsCount; j--) {
			if (j < 5) {
				LinearLayout firstFloor = (LinearLayout) findViewById(R.id.first_floor);
				ImageButton imgButton = (ImageButton) firstFloor.getChildAt(j);

				imgButton.setClickable(false);

			} else if (j >= 5 && j < 10) {
				LinearLayout firstFloor = (LinearLayout) findViewById(R.id.second_floor);
				ImageButton imgButton = (ImageButton) firstFloor
						.getChildAt(j - 5);
				imgButton.setClickable(false);

			} else if (j >= 10 && j < 15) {
				LinearLayout firstFloor = (LinearLayout) findViewById(R.id.third_floor);
				ImageButton imgButton = (ImageButton) firstFloor
						.getChildAt(j - 10);
				imgButton.setClickable(false);

			} else if (j >= 15 && j < 20) {
				LinearLayout firstFloor = (LinearLayout) findViewById(R.id.forth_floor);
				ImageButton imgButton = (ImageButton) firstFloor
						.getChildAt(j - 15);
				imgButton.setClickable(false);
			}
		}
	}

	@Override
	protected void onPause() {
		MenuActivity.m_BackgroundMusicPlayer.stop();
		super.onPause();
	}

	/**
	 * Set up the ActionBar.
	 */
	private void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.building, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Reacts on clicks to the window and open the correct room if it is not
	 * closed.
	 * 
	 * @param v
	 *            view where the player clicked on
	 */
	public void onWindowClick(View v) {
		if (MenuActivity.m_BackgroundMusicPlayer.isPlaying()) {
			MenuActivity.m_BackgroundMusicPlayer.stop();
		}

		Intent goToFloorIntent = new Intent().setClass(getBaseContext(),
				FloorActivity.class);

		switch (v.getId()) {
		case R.id.settings_back:
			// get floor nr (if non exist, set it to one)
			goToFloorIntent.putExtra("floor", 1);
			goToFloorIntent.putExtra("room", 0);
			break;
		case R.id.button2:
			goToFloorIntent.putExtra("floor", 1);
			goToFloorIntent.putExtra("room", 1);
			break;
		case R.id.button3:
			goToFloorIntent.putExtra("floor", 1);
			goToFloorIntent.putExtra("room", 2);
			break;
		case R.id.button4:
			goToFloorIntent.putExtra("floor", 1);
			goToFloorIntent.putExtra("room", 3);
			break;
		case R.id.button5:
			goToFloorIntent.putExtra("floor", 1);
			goToFloorIntent.putExtra("room", 4);
			break;
		case R.id.button6:
			goToFloorIntent.putExtra("floor", 2);
			goToFloorIntent.putExtra("room", 0);
			break;
		case R.id.button7:
			goToFloorIntent.putExtra("floor", 2);
			goToFloorIntent.putExtra("room", 1);
			break;
		case R.id.button8:
			goToFloorIntent.putExtra("floor", 2);
			goToFloorIntent.putExtra("room", 2);
			break;
		case R.id.button9:
			goToFloorIntent.putExtra("floor", 2);
			goToFloorIntent.putExtra("room", 3);
			break;
		case R.id.button10:
			goToFloorIntent.putExtra("floor", 2);
			goToFloorIntent.putExtra("room", 4);
			break;
		case R.id.button11:
			goToFloorIntent.putExtra("floor", 3);
			goToFloorIntent.putExtra("room", 0);
			break;
		case R.id.button12:
			goToFloorIntent.putExtra("floor", 3);
			goToFloorIntent.putExtra("room", 1);
			break;
		case R.id.button13:
			goToFloorIntent.putExtra("floor", 3);
			goToFloorIntent.putExtra("room", 2);
			break;
		case R.id.button14:
			goToFloorIntent.putExtra("floor", 3);
			goToFloorIntent.putExtra("room", 3);
			break;
		case R.id.button15:
			goToFloorIntent.putExtra("floor", 3);
			goToFloorIntent.putExtra("room", 4);
			break;
		case R.id.button16:
			goToFloorIntent.putExtra("floor", 4);
			goToFloorIntent.putExtra("room", 0);
			break;
		case R.id.button17:
			goToFloorIntent.putExtra("floor", 4);
			goToFloorIntent.putExtra("room", 1);
			break;
		case R.id.button18:
			goToFloorIntent.putExtra("floor", 4);
			goToFloorIntent.putExtra("room", 2);
			break;
		case R.id.button19:
			goToFloorIntent.putExtra("floor", 4);
			goToFloorIntent.putExtra("room", 3);
			break;
		case R.id.button20:
			goToFloorIntent.putExtra("floor", 4);
			goToFloorIntent.putExtra("room", 4);
			break;
		}

		startActivity(goToFloorIntent);
	}
}
